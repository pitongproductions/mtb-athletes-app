import AppConstant from "../constants/AppConstant";
import AthleteInterface from "../interfaces/AthleteInterface";

export default class Athlete {

    id: number;
    firstName: string;
    lastName: string;
    description: string;
    profileImageUrl: string;
    coverImageUrl: string;
    photoUrls: string;
    createdAt: string;
    updatedAt: string;

    constructor() {
        this.id = 0;
        this.firstName = "";
        this.lastName = "";
        this.description = "";
        this.profileImageUrl = "";
        this.coverImageUrl = "";
        this.photoUrls = "";
        this.createdAt = "";
        this.updatedAt = "";
    }

    setId(id: number) {
        this.id = id;
    }

    getId() {
        return this.id;
    }

    setFirstName(firstName: string) {
        this.firstName = firstName;
    }

    getFirstName() {
        return this.firstName;
    }

    setLastName(lastName: string) {
        this.lastName = lastName;
    }

    getLastName() {
        return this.lastName;
    }

    getFullName() {
        return `${this.getFirstName()} ${this.getLastName()}`;
    }

    setDescription(description: string) {
        this.description = description;
    }

    getDescription() {
        return this.description;
    }

    setProfileImageUrl(profileImageUrl: string) {
        this.profileImageUrl = profileImageUrl;
    }

    getProfileImageUrl() {
        if (this.profileImageUrl === "") {
            return AppConstant.DEFAULT_USER_PROFILE_IMAGE;
        }

        return this.profileImageUrl;
    }

    setCoverImageUrl(coverImageUrl: string) {
        this.coverImageUrl = coverImageUrl;
    }

    getCoverImageUrl() {
        if (this.coverImageUrl === "") {
            return AppConstant.DEFAULT_USER_COVER_IMAGE;
        }

        return this.coverImageUrl ?? AppConstant.DEFAULT_USER_COVER_IMAGE;
    }

    setPhotoUrls(photoUrls: string) {
        this.photoUrls = photoUrls;
    }

    getPhotoUrls() {
        if (this.photoUrls === "") {
            return [];
        }

        let photoUrlList = this.photoUrls.split(AppConstant.DEFAULT_DELIMITER);

        return photoUrlList.filter(photo => photo !== "");
    }

    isEmpty() {
        return this.id === 0;
    }

    setCreatedAt(createdAt: string) {
        this.createdAt = createdAt;
    }

    getCreatedAt() {
        return this.createdAt;
    }

    setUpdatedAt(updatedAt: string) {
        this.updatedAt = updatedAt;
    }

    getUpdatedAt() {
        return this.updatedAt;
    }

    toObject(data: AthleteInterface) {
        if (data) {
            this.setId(data.id ?? this.id);
            this.setFirstName(data.first_name ?? this.firstName);
            this.setLastName(data.last_name ?? this.lastName);
            this.setDescription(data.description ?? this.description);
            this.setProfileImageUrl(data.profile_image_url ?? this.profileImageUrl);
            this.setCoverImageUrl(data.cover_image_url ?? this.coverImageUrl);
            this.setPhotoUrls(data.photo_urls ?? this.photoUrls);
            this.setCreatedAt(data.created_at ?? this.createdAt);
            this.setUpdatedAt(data.updated_at ?? this.updatedAt);
        }

        return this;
    }
}
