import PaginationInterface from "../interfaces/PaginationInterface";

export default class Pagination {

    currentPage: number;
    totalPages: number;
    totalRecords: number;

    constructor() {
        this.currentPage = 0;
        this.totalPages = 0;
        this.totalRecords = 0;
    }

    setCurrentPage(currentPage: number) {
        this.currentPage = currentPage;
    }

    getCurrentPage() {
        return this.currentPage;
    }

    setTotalPages(totalPages: number) {
        this.totalPages = totalPages;
    }

    getTotalPages() {
        return this.totalPages;
    }

    setTotalRecords(totalRecords: number) {
        this.totalRecords = totalRecords;
    }

    getTotalRecords() {
        return this.totalRecords;
    }

    toObject(data: PaginationInterface) {
        if (data) {
            this.setCurrentPage(data.current_page ?? this.currentPage);
            this.setTotalPages(data.last_page ?? this.totalPages);
            this.setTotalRecords(data.total ?? this.totalRecords);
        }

        return this;
    }
}
