import React from "react";
import Router from "./Router";
import NavBar from "./NavBar";

export default class App extends React.Component {
    render() {
        return (
            <div className="App">
                <NavBar/>
                <div className="mb-32">
                    <Router/>
                </div>
            </div>
        );
    }
}
