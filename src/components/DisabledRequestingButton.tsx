import React from "react";

export default class DisabledRequestingButton extends React.Component {
    render() {
        return (
            <div>
                <button
                    type="button"
                    className="bg-white border border-blue-500 uppercase text-blue-500 font-bold py-2 px-8 focus:outline-none rounded-full"
                    disabled
                >
                    <i className="fa fa-spinner fa-spin"/> Processing
                </button>
            </div>
        );
    }
}
