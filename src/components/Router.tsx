import React from "react";
import {
    BrowserRouter,
    Switch,
    Route
} from "react-router-dom";
import Routes from "../routes/Routes";

export default class Router extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <div>
                    <Switch>
                        {Routes.config.map((route: any, i: number) => (
                            <Route
                                key={i}
                                path={route.path}
                                exact={route.exact}
                                render={props => (
                                    <route.component {...props} routes={route.routes}/>
                                )}/>
                        ))}
                    </Switch>
                </div>
            </BrowserRouter>
        );
    }
}

