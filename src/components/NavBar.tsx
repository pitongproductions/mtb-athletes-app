import React from "react";
import {
    BrowserRouter,
    Link
} from "react-router-dom";
import AppConstant from "../constants/AppConstant";

export default class NavBar extends React.Component {
    render() {
        return (
            <div className="p-5 bg-blue-500 text-white font-bold shadow-xl">
                <BrowserRouter>
                    <ul>
                        <li>
                            <Link to="/">
                                <h1 className="text-2xl">{AppConstant.APP_NAME}</h1>
                            </Link>
                        </li>
                    </ul>
                </BrowserRouter>
            </div>
        );
    }
}
