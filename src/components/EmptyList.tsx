import React from "react";

export default class EmptyList extends React.Component {
    render() {
        return (
            <div className="text-center">
                <p>List is empty.</p>
            </div>
        );
    }
}
