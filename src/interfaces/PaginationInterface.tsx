export default interface PaginationInterface {
    current_page: number;
    last_page: number;
    total: number;
}
