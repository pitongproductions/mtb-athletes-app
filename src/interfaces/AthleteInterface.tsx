export default interface AthleteInterface {
    id: number;
    first_name: string;
    last_name: string;
    description: string;
    profile_image_url: string;
    cover_image_url: string;
    photo_urls: string;
    created_at: string;
    updated_at: string;
}
