export default class RequestMethodConstant {

    static readonly POST: string = "POST";
    static readonly GET: string = "GET";
    static readonly DELETE: string = "DELETE";
    static readonly PUT: string = "PUT";
    static readonly PATCH: string = "PATCH";
    static readonly HTTP_HEADER_MULTIPART: Object = {
        header: {
            "Content-Type": "multipart/form-data"
        }
    }

    static asList() {
        return [
            this.POST,
            this.GET,
            this.DELETE,
            this.PUT,
            this.PATCH
        ];
    }
}
