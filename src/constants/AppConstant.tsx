import userProfile from "../assets/images/avatar_48dp.png";
import userCover from "../assets/images/cover.jpg";

export default class AppConstant {

    static readonly APP_NAME: string = process.env.REACT_APP_NAME ?? "AppName";
    static readonly DEFAULT_DELIMITER: string = "^";
    static readonly DEFAULT_USER_PROFILE_IMAGE: string = userProfile;
    static readonly DEFAULT_USER_COVER_IMAGE: string = userCover;
    static readonly DEFAULT_SWEET_ALERT_TIMER: number = 2000;
}
