import React from "react";

export default class Error404 extends React.Component {
    render() {
        return (
            <div className="text-center p-5 mt-10">
                <h1 className="text-3xl">404 Page Not Found</h1>
            </div>
        );
    }
}
