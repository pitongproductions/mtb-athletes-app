import React from "react";
import {
    Link
} from "react-router-dom";
import {connect} from "react-redux";
import Avatar from "react-avatar";
import AthleteService from "../../services/AthleteService";
import EmptyList from "../../components/EmptyList";
import Athlete from "../../models/Athlete";
import AddOrEditAthleteDialog from "./dialogs/AddOrEditAthleteDialog";
import StoreConstantDialog from "../../store/constants/StoreConstantDialog";
import DialogUtil from "../../utils/DialogUtil";

interface Props {
    mainReducer: {
        isRequesting: boolean
    },
    dialogReducer: Object,
    athleteReducer: {
        athleteList: []
    },
    history: {
        push: Function
    },
    fetchAthletes: Function
}

class AthleteList extends React.Component<Props> {
    state = {
        athleteService: new AthleteService(),
        athleteList: [],
        page: 1,
        search: "",
        sortBy: ""
    }

    render() {
        return (
            <div className="p-5 flex">
                <div className="w-full sm:w-4/5 lg:w-3/5 m-auto">
                    <div className="w-full inline-flex">
                        <h1 className="text-3xl font-bold mb-4">Riders</h1>
                        <button
                            type="button"
                            className="p-2 text-blue-500 text-sm font-bold focus:outline-none ml-auto"
                            onClick={() => DialogUtil.setToggledDialog(StoreConstantDialog.ADD_OR_EDIT_ATHLETE_DIALOG)}
                        >
                            <i className="fa fa-plus"/> Add Athlete
                        </button>
                    </div>
                    <div className="mt-4 mb-4">
                        <form
                            onSubmit={this.searchAthletes()}
                            className="sm:flex items-center"
                        >
                            <div>
                                <input
                                    type="text"
                                    className="appearance-none border font-light rounded w-full p-2 text-gray-700 leading-tight focus:outline-none border-gray-500 focus:border-blue-400"
                                    placeholder="Search Athletes"
                                    onChange={this.handleSearchOnChange()}
                                />
                            </div>
                            <div className="sm:flex items-center mt-4 sm:mt-0 sm:ml-6">
                                <h1 className="text-lg">Sort by:</h1>
                                <label className="sm:ml-2 cursor-pointer">
                                    <input
                                        type="checkbox"
                                        className="mr-2 leading-tight"
                                        name="first_name"
                                        onChange={this.handleSortOnChange()}
                                        checked={this.state.sortBy === "first_name"}
                                    />
                                    <span className="text-sm">Firstname</span>
                                </label>
                                <label className="ml-2 cursor-pointer">
                                    <input
                                        type="checkbox"
                                        className="mr-2 leading-tight"
                                        name="last_name"
                                        onChange={this.handleSortOnChange()}
                                        checked={this.state.sortBy === "last_name"}
                                    />
                                    <span className="text-sm">Lastname</span>
                                </label>
                            </div>
                        </form>
                    </div>
                    {
                        this.props.mainReducer.isRequesting ?
                            <div className="text-center mt-20">
                                <i className="fa fa-spinner fa-spin text-2xl"/>
                            </div> : ""
                    }
                    {
                        this.state.athleteList.map((athlete: Athlete, index: number) => (
                            <div
                                key={index}
                                className="py-1"
                            >
                                <Link to={`athletes/${athlete.getId()}`}>
                                    <div
                                        className="w-full p-3 rounded border border-gray-400 cursor-pointer">
                                        <div className="flex items-center">
                                            <Avatar
                                                name={athlete.getFullName()}
                                                src={athlete.getProfileImageUrl()}
                                                size="60"
                                                round={true}
                                            />
                                            <h1 className="text-xl ml-6">{athlete.getFullName()}</h1>
                                        </div>
                                    </div>
                                </Link>
                            </div>
                        ))}
                    {
                        this.state.athleteList.length > 0 ?
                            <div className="flex mt-4">
                                <div className="flex items-center mr-auto">
                                    <h1>Page {this.state.athleteService.pagination.getCurrentPage()} of {this.state.athleteService.pagination.getTotalPages()}</h1>
                                    <div className="ml-4">
                                        {
                                            this.state.athleteService.pagination.getCurrentPage() > 1 ?
                                                <button
                                                    type="button"
                                                    className="px-2 py-1 text-xs border border-gray-500 focus:outline-none rounded"
                                                    onClick={this.goToPage(this.state.page - 1)}
                                                >
                                                    <i className="fa fa-angle-left"/>
                                                </button> : ""
                                        }
                                        {
                                            this.state.athleteService.pagination.getCurrentPage() < this.state.athleteService.pagination.getTotalPages() ?
                                                <button
                                                    type="button"
                                                    className="px-2 py-1 text-xs border border-gray-500 focus:outline-none rounded ml-2"
                                                    onClick={this.goToPage(this.state.page + 1)}
                                                >
                                                    <i className="fa fa-angle-right"/>
                                                </button> : ""
                                        }
                                    </div>
                                </div>
                                <div className="ml-auto">
                                    <h1>{this.state.athleteService.pagination.getTotalRecords()} Athletes</h1>
                                </div>
                            </div> : ""
                    }
                    {
                        !this.props.mainReducer.isRequesting && this.state.athleteList.length === 0 ?
                            <div className="mt-10">
                                <EmptyList/>
                            </div> : ""
                    }
                </div>
                {
                    DialogUtil.isDialogToggled(StoreConstantDialog.ADD_OR_EDIT_ATHLETE_DIALOG) ?
                        <AddOrEditAthleteDialog
                            athlete={new Athlete()}
                            history={this.props.history}
                            mainReducer={this.props.mainReducer}
                            setAthlete={() => {}}
                        /> : ""
                }
            </div>
        );
    }

    componentDidMount() {
        this.loadAthletes();
    }

    handleSearchOnChange = () => (event: React.ChangeEvent<HTMLInputElement>) => {
        event.preventDefault();
        this.setState({search: event.target.value});
    }

    handleSortOnChange = () => (event: React.ChangeEvent<HTMLInputElement>) => {
        event.preventDefault();

        const sortByName = event.target.name;
        let sortBy = this.state.sortBy === sortByName ? "" : sortByName;

        this.setState({sortBy: sortBy}, () => {
            this.loadAthletes();
        });

    }

    searchAthletes = () => (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        this.loadAthletes();
    }

    goToPage = (targetPage: number = 1) => (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
        this.setState({page: targetPage}, () => {
            this.loadAthletes();
        });
    }

    loadAthletes() {
        let payload = {
            page: this.state.page,
            search: this.state.search,
            sortBy: this.state.sortBy
        };

        this.state.athleteService.getAthletes(payload)
            .then(() => {
                this.setState({athleteList: [...this.state.athleteService.athleteList]});
            });
    }
}

const mapStateToProps = (state: any) => ({
    mainReducer: state.mainReducer,
    dialogReducer: state.dialogReducer,
    athleteReducer: state.athleteReducer
});

export default connect(mapStateToProps, null)(AthleteList);
