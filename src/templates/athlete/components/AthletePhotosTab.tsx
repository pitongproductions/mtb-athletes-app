import React from "react";
import Athlete from "../../../models/Athlete";
import EmptyList from "../../../components/EmptyList";
import DialogUtil from "../../../utils/DialogUtil";
import StoreConstantDialog from "../../../store/constants/StoreConstantDialog";
import UploadImagesDialog from "../dialogs/UploadImagesDialog";
import PhotoViewer from "../dialogs/PhotoViewer";

interface Props {
    mainReducer: {
        isRequesting: boolean
    },
    dialogReducer: Object,
    athlete: Athlete;
    setAthlete: Function;
}

export default class AthletePhotosTab extends React.Component<Props> {
    state = {
        activePhotoUrl: this.props.athlete.getPhotoUrls()[0]
    };

    render() {
        return (
            <div>
                <div className="flex mb-10">
                    <button
                        type="button"
                        className="bg-white border border-blue-500 m-auto uppercase text-xs text-blue-500 font-bold py-2 px-8 focus:outline-none rounded-full"
                        onClick={() => {
                            DialogUtil.setToggledDialog(StoreConstantDialog.UPLOAD_IMAGES)
                        }}
                    >
                        <i className="fa fa-image"/> Upload Photos
                    </button>
                </div>
                {
                    this.props.athlete.getPhotoUrls().length > 0 ?
                        <div>
                            {
                                this.props.athlete.getPhotoUrls().map((photoUrl: string, index: number) => (
                                    <div
                                        key={index}
                                        className="w-full sm:w-1/2 lg:w-4/12 inline-block p-1 cursor-pointer"
                                        onClick={() => {this.viewPhotos(photoUrl)}}
                                    >
                                        <img src={photoUrl} className="rounded shadow-lg" alt=""/>
                                    </div>
                                ))
                            }
                        </div> : <EmptyList/>
                }
                {
                    DialogUtil.isDialogToggled(StoreConstantDialog.UPLOAD_IMAGES) ?
                        <UploadImagesDialog
                            mainReducer={this.props.mainReducer}
                            athlete={this.props.athlete}
                            setAthlete={this.props.setAthlete}
                        /> : ""
                }
                {
                    DialogUtil.isDialogToggled(StoreConstantDialog.VIEW_PHOTOS_DIALOG) ?
                        <PhotoViewer
                            mainReducer={this.props.mainReducer}
                            athlete={this.props.athlete}
                            activePhotoUrl={this.state.activePhotoUrl}
                            setAthlete={this.props.setAthlete}
                        /> : ""
                }
            </div>
        );
    }

    viewPhotos(photoUrl: string) {
        this.setState({activePhotoUrl: photoUrl});
        DialogUtil.setToggledDialog(StoreConstantDialog.VIEW_PHOTOS_DIALOG);
    }
}
