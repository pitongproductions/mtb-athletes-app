import React from "react";
import Athlete from "../../../models/Athlete";
import EmptyList from "../../../components/EmptyList";

interface Props {
    athlete: Athlete;
}

export default class AthleteVideosTab extends React.Component<Props> {
    state = {
        videos: []
    };

    render() {
        return (
            <div>
                {
                    this.state.videos.length > 0 ?
                        <div>
                            <h1>List of Videos here...</h1>
                        </div> : <EmptyList/>
                }
            </div>
        );
    }
}
