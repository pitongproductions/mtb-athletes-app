import React from "react";
import Athlete from "../../../models/Athlete";
import DialogUtil from "../../../utils/DialogUtil";
import StoreConstantDialog from "../../../store/constants/StoreConstantDialog";
import DeleteAthleteDialog from "../dialogs/DeleteAthleteDialog";

interface Props {
    mainReducer: {
        isRequesting: boolean
    },
    dialogReducer: Object,
    history: {
        push: Function
    },
    athlete: Athlete
}

export default class AthleteSettingsTab extends React.Component<Props> {
    render() {
        return (
            <div>
                <div className="py-4">
                    <h1 className="text-xl text-gray-600">Account Ownership</h1>
                    <button
                        type="button"
                        className="bg-red-500 text-white text-xs border border-red-500 uppercase font-bold py-1 px-4 focus:outline-none rounded mt-2"
                        onClick={() => {DialogUtil.setToggledDialog(StoreConstantDialog.DELETE_ATHLETE_DIALOG)}}
                    >
                        <i className="fa fa-trash"/> Delete Athlete
                    </button>
                </div>
                {
                    DialogUtil.isDialogToggled(StoreConstantDialog.DELETE_ATHLETE_DIALOG) ?
                        <DeleteAthleteDialog
                            mainReducer={this.props.mainReducer}
                            history={this.props.history}
                            athlete={this.props.athlete}
                        /> : ""
                }
            </div>
        );
    }
}
