import React from "react";
import Athlete from "../../../models/Athlete";

interface Props {
    athlete: Athlete;
}

export default class AthleteAboutTab extends React.Component<Props> {
    render() {
        return (
            <div>
                {
                    this.props.athlete.getDescription() !== "" ?
                        <h1>{this.props.athlete.getDescription()}</h1> : <p>Nothing here.</p>
                }
            </div>
        );
    }
}
