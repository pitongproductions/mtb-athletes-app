import React from "react";
import Athlete from "../../../models/Athlete";
import DisabledRequestingButton from "../../../components/DisabledRequestingButton";
import DialogUtil from "../../../utils/DialogUtil";
import FileUtil from "../../../utils/FileUtil";
import AthleteService from "../../../services/AthleteService";
import AlertUtil from "../../../utils/AlertUtil";

interface Props {
    mainReducer: {
        isRequesting: boolean
    },
    athlete: Athlete,
    activePhotoUrl: string,
    setAthlete: Function
}

export default class PhotoViewer extends React.Component<Props> {
    state = {
        athleteService: new AthleteService(),
        photoUrls: this.props.athlete.getPhotoUrls(),
        activePhotoUrl: this.props.activePhotoUrl
    };

    render() {
        return (
            <div className="modal fixed w-full h-full top-0 left-0 flex items-center z-20">
                <div className="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"/>
                <div className="w-full px-2 z-50">
                    <div className="modal-container bg-white sm:max-w-sm mx-auto rounded shadow-lg">
                        <div
                            style={FileUtil.setBackgroundImage(this.state.activePhotoUrl)}
                            className="modal-content h-64 overflow-y-auto p-6 text-center relative"
                        >
                            <div className="flex text-3xl text-gray-200">
                                {
                                    this.getIndexOfActivePhoto() > 0 ?
                                        <button
                                            type="button"
                                            className="mr-auto focus:outline-none"
                                            onClick={() => {
                                                this.setActivePhotoUrlByIndex(this.getIndexOfActivePhoto() - 1)
                                            }}
                                        >
                                            <i className="fa fa-angle-left"/>
                                        </button> : ""
                                }
                                {
                                    this.getIndexOfActivePhoto() < (this.state.photoUrls.length - 1) ?
                                        <button
                                            type="button"
                                            className="ml-auto focus:outline-none"
                                            onClick={() => {
                                                this.setActivePhotoUrlByIndex(this.getIndexOfActivePhoto() + 1)
                                            }}
                                        >
                                            <i className="fa fa-angle-right"/>
                                        </button> : ""
                                }
                            </div>
                            <div className="flex mt-4 text-xs text-center">
                                {
                                    this.props.mainReducer.isRequesting ?
                                        <DisabledRequestingButton/> : <button
                                            type="button"
                                            className="absolute bottom-0 mb-4 bg-red-500 border border-red-500 uppercase text-white font-bold w-10 h-10 focus:outline-none rounded-full"
                                            onClick={() => {
                                                this.deleteAthletePhoto()
                                            }}
                                        >
                                            <i className="fa fa-trash" />
                                        </button>
                                }
                            </div>
                        </div>
                        <div className="modal-footer p-6">
                            <div className="flex">
                                <div className="flex m-auto text-xs">
                                    <button
                                        type="button"
                                        className="bg-white border border-blue-500 uppercase text-blue-500 font-bold py-2 px-8 focus:outline-none rounded-full"
                                        onClick={() => {
                                            DialogUtil.setToggledDialog("");
                                        }}
                                    >
                                        Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    setActivePhotoUrlByIndex(indexOfPhotoUrl: number) {
        this.setState({activePhotoUrl: this.state.photoUrls[indexOfPhotoUrl]});
    }

    getIndexOfActivePhoto() {
        return this.state.photoUrls.indexOf(this.state.activePhotoUrl);
    }

    deleteAthletePhoto() {
        let payload = {
            photoUrl: this.state.activePhotoUrl
        };

        this.state.athleteService.deleteAthletePhoto(this.props.athlete, payload)
            .then(() => {
                const athlete = this.state.athleteService.athlete;
                const responseMessage = this.state.athleteService.responseMessage;

                if (!athlete.isEmpty()) {
                    this.props.setAthlete(athlete);
                }

                DialogUtil.setToggledDialog("");
                AlertUtil.success(responseMessage);
            });
    }
}
