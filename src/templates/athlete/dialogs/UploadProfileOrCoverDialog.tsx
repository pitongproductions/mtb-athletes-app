import React from "react";
import DisabledRequestingButton from "../../../components/DisabledRequestingButton";
import DialogUtil from "../../../utils/DialogUtil";
import Avatar from "react-avatar-edit";
import FileUtil from "../../../utils/FileUtil";
import AthleteService from "../../../services/AthleteService";
import Athlete from "../../../models/Athlete";
import AlertUtil from "../../../utils/AlertUtil";

interface Props {
    mainReducer: {
        isRequesting: boolean
    },
    dialogType: string,
    athlete: Athlete,
    setAthlete: Function
}

export default class UploadProfileOrCoverDialog extends React.Component<Props> {
    state = {
        athleteService: new AthleteService(),
        fileName: "",
        fileType: "",
        isProfileUpload: this.props.dialogType === "Profile",
        previewUploadImage: false,
        previewImageDataUrl: "",
        croppedImageDataUrl: ""
    };

    constructor(props: Props) {
        super(props);
        this.onClose = this.onClose.bind(this);
        this.onCrop = this.onCrop.bind(this);
        this.onFileLoad = this.onFileLoad.bind(this);
        this.uploadProfileImage = this.uploadProfileImage.bind(this);
    }

    render() {
        return (
            <div className="modal fixed w-full h-full top-0 left-0 flex items-center z-20">
                <div className="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"/>
                <div className="w-full px-2 z-50">
                    <div
                        className={`${this.state.isProfileUpload ? "sm:max-w-sm" : "sm:max-w-xl"} modal-container bg-white mx-auto rounded shadow-lg`}>
                        <div className="modal-header text-center p-6 bg-blue-500">
                            <h1 className="text-white font-bold uppercase">Upload {this.props.dialogType} Image</h1>
                        </div>
                        <div className="modal-content overflow-y-auto p-6 flex">
                            <div className="m-auto">
                                {
                                    this.state.previewUploadImage || (this.state.croppedImageDataUrl && !this.state.isProfileUpload) ?
                                        <div>
                                            <img
                                                src={this.state.isProfileUpload ? this.state.croppedImageDataUrl : this.state.previewImageDataUrl}
                                                className={`${this.state.isProfileUpload ? "w-50 h-50 rounded-full" : "w-full"}`}
                                                alt=""
                                            />
                                        </div> : <div>
                                            <Avatar
                                                width={300}
                                                height={250}
                                                imageWidth={300}
                                                onCrop={this.onCrop}
                                                onClose={this.onClose}
                                                onFileLoad={this.onFileLoad}
                                                src={this.state.previewImageDataUrl}
                                                label="Click to upload"
                                            />
                                        </div>
                                }
                                {
                                    this.state.isProfileUpload ?
                                        <div className="flex mt-6">
                                            <button
                                                type="button"
                                                className="bg-white text-xs uppercase text-blue-500 font-bold py-1 px-8 focus:outline-none rounded-full m-auto"
                                                onClick={this.inverseProfileImagePreviewStatus()}
                                            >
                                                {this.state.previewUploadImage ? "Crop" : "Preview"}
                                            </button>
                                        </div> : ""
                                }
                            </div>
                        </div>
                        <div className="modal-footer flex p-6">
                            <div className="flex m-auto">
                                <div className="mr-2">
                                    {
                                        this.props.mainReducer.isRequesting ?
                                            <DisabledRequestingButton/> : <button
                                                type="submit"
                                                className="bg-blue-500 text-white uppercase font-bold py-2 px-8 focus:outline-none rounded-full"
                                                onClick={this.uploadProfileImage}
                                                disabled={this.state.croppedImageDataUrl === ""}
                                            >
                                                Upload
                                            </button>
                                    }
                                </div>
                                <button
                                    type="button"
                                    className="bg-white border border-blue-500 uppercase text-blue-500 font-bold py-2 px-8 focus:outline-none rounded-full"
                                    onClick={() => {
                                        DialogUtil.setToggledDialog("")
                                    }}
                                >
                                    Cancel
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    onClose() {
        this.inverseProfileImagePreviewStatus();
    }

    onCrop(imageDataUrl: string) {
        this.setState({croppedImageDataUrl: imageDataUrl});
    }

    onFileLoad(file: any) {
        this.setState({
            fileName: file.name,
            fileType: file.type
        });

        FileUtil.readAsDataUrl(file, (dataUrl: string) => {
            this.setState({previewImageDataUrl: dataUrl});
        });
    }

    inverseProfileImagePreviewStatus = () => (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        event.preventDefault();
        const status = !this.state.previewUploadImage;
        this.setState({previewUploadImage: status});
    }

    uploadProfileImage() {
        if (this.props.mainReducer.isRequesting) {
            return;
        }

        const file = FileUtil.urlToFile(
            this.state.isProfileUpload ? this.state.croppedImageDataUrl : this.state.previewImageDataUrl,
            this.state.fileName,
            this.state.fileType
        );

        let payload = new FormData();
        payload.append("file", file);
        payload.append("type", `${this.props.dialogType.toLowerCase()}`);

        this.state.athleteService.uploadAthleteProfileImage(this.props.athlete, payload)
            .then(() => {
                const athlete = this.state.athleteService.athlete;
                const responseMessage = this.state.athleteService.responseMessage;

                if (!responseMessage) {
                    return;
                }

                this.props.setAthlete(athlete);
                DialogUtil.setToggledDialog("");
                AlertUtil.success(responseMessage);
            });
    }
}
