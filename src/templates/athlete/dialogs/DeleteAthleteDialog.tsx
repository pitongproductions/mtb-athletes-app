import React from "react";
import AthleteService from "../../../services/AthleteService";
import DisabledRequestingButton from "../../../components/DisabledRequestingButton";
import DialogUtil from "../../../utils/DialogUtil";
import AlertUtil from "../../../utils/AlertUtil";
import Athlete from "../../../models/Athlete";
import Avatar from "react-avatar";

interface Props {
    mainReducer: {
        isRequesting: boolean
    },
    history: {
        push: Function
    },
    athlete: Athlete
}

export default class DeleteAthleteDialog extends React.Component<Props> {
    state = {
        athleteService: new AthleteService()
    };

    render() {
        return (
            <div className="modal fixed w-full h-full top-0 left-0 flex items-center z-20">
                <div className="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"/>
                <div className="w-full px-2 z-50">
                    <div className="modal-container bg-white sm:max-w-sm mx-auto rounded shadow-lg">
                        <div className="modal-header text-center p-6 bg-blue-500">
                            <h1 className="text-white font-bold uppercase">Confirm Delete Athlete</h1>
                        </div>
                        <div className="modal-content overflow-y-auto p-6 text-center">
                            <Avatar
                                name={this.props.athlete.getFullName()}
                                src={this.props.athlete.getProfileImageUrl()}
                                size="100"
                                className="rounded cursor-pointer border border-gray-500"
                                round={true}
                            />
                            <h1 className="text-2xl mt-3">{this.props.athlete.getFullName()}</h1>
                        </div>
                        <div className="modal-footer p-6">
                            <div className="flex">
                                <div className="flex m-auto">
                                    <div className="mr-2">
                                        {
                                            this.props.mainReducer.isRequesting ?
                                                <DisabledRequestingButton/> : <button
                                                    type="button"
                                                    className="bg-red-500 text-white uppercase font-bold py-2 px-8 focus:outline-none rounded-full"
                                                    onClick={() => {this.deleteAthlete()}}
                                                >
                                                    Delete
                                                </button>
                                        }
                                    </div>
                                    <button
                                        type="button"
                                        className="bg-white border border-blue-500 uppercase text-blue-500 font-bold py-2 px-8 focus:outline-none rounded-full"
                                        onClick={() => {
                                            DialogUtil.setToggledDialog("");
                                        }}
                                    >
                                        Cancel
                                    </button>
                                </div>
                            </div>
                            <div className="text-center mt-6">
                                <p className="text-xs text-gray-600">Cannot be undone!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    deleteAthlete() {
        this.state.athleteService.deleteAthlete(this.props.athlete)
            .then(() => {
                const responseMessage = this.state.athleteService.responseMessage;
                DialogUtil.setToggledDialog("");
                AlertUtil.success(responseMessage, () => {
                    this.props.history.push("/");
                });
            });
    }
}
