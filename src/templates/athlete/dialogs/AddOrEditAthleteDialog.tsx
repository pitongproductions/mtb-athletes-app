import React from "react";
import AthleteService from "../../../services/AthleteService";
import DisabledRequestingButton from "../../../components/DisabledRequestingButton";
import DialogUtil from "../../../utils/DialogUtil";
import AlertUtil from "../../../utils/AlertUtil";
import Athlete from "../../../models/Athlete";

interface Props {
    history: {
        push: Function
    };
    mainReducer: {
        isRequesting: boolean
    };
    athlete: Athlete,
    setAthlete: Function
}

export default class AddOrEditAthleteDialog extends React.Component<Props> {
    state = {
        athleteService: new AthleteService(),
        validated: false,
        firstName: this.props.athlete.getFirstName(),
        lastName: this.props.athlete.getLastName(),
        description: this.props.athlete.getDescription()
    };

    render() {
        return (
            <div className="modal fixed w-full h-full top-0 left-0 flex items-center z-20">
                <div className="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"/>
                <div className="w-full px-2 z-50">
                    <div className="modal-container bg-white sm:max-w-sm mx-auto rounded shadow-lg">
                        <form onSubmit={this.saveAthlete()}>
                            <div className="modal-header text-center p-6 bg-blue-500">
                                <h1 className="text-white font-bold uppercase">{this.props.athlete.isEmpty() ? "Add" : "Edit"} Athlete</h1>
                            </div>
                            <div className="modal-content overflow-y-auto p-6">
                                <div className="mt-4">
                                    <input
                                        type="text"
                                        id="firstName"
                                        className="appearance-none border font-light rounded w-full p-2 text-gray-700 leading-tight focus:outline-none border-gray-500 focus:border-blue-400"
                                        placeholder="Firstname"
                                        onChange={this.handleInputOnChange()}
                                        value={this.state.firstName}
                                        required
                                    />
                                </div>
                                <div className="mt-4">
                                    <input
                                        type="text"
                                        id="lastName"
                                        className="appearance-none border font-light rounded w-full p-2 text-gray-700 leading-tight focus:outline-none border-gray-500 focus:border-blue-400"
                                        placeholder="Lastname"
                                        onChange={this.handleInputOnChange()}
                                        value={this.state.lastName}
                                        required
                                    />
                                </div>
                                <div className="mt-4">
                                    <textarea
                                        id="description"
                                        className="appearance-none border font-light rounded w-full p-2 text-gray-700 leading-tight focus:outline-none border-gray-500 focus:border-blue-400"
                                        placeholder="Description"
                                        onChange={this.handleInputOnChange()}
                                        value={this.state.description}
                                        required
                                    />
                                </div>
                            </div>
                            <div className="modal-footer flex p-6">
                                <div className="flex m-auto">
                                    <div className="mr-2">
                                        {
                                            this.props.mainReducer.isRequesting ?
                                                <DisabledRequestingButton/> : <button
                                                    type="submit"
                                                    className="bg-blue-500 text-white uppercase font-bold py-2 px-8 focus:outline-none rounded-full"
                                                >
                                                    Save
                                                </button>
                                        }
                                    </div>
                                    <button
                                        type="button"
                                        className="bg-white border border-blue-500 uppercase text-blue-500 font-bold py-2 px-8 focus:outline-none rounded-full"
                                        onClick={() => {
                                            DialogUtil.setToggledDialog("");
                                        }}
                                    >
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

    saveAthlete = () => (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        if (this.props.mainReducer.isRequesting) {
            return;
        }

        let payload = {
            id: 0,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            description: this.state.description
        };

        if (!this.props.athlete.isEmpty()) {
            payload.id = this.props.athlete.getId();
        }

        this.state.athleteService.saveAthlete(payload)
            .then(() => {
                const athlete = this.state.athleteService.athlete;
                const responseMessage = this.state.athleteService.responseMessage;

                if (!this.props.athlete.isEmpty()) {
                    this.props.setAthlete(athlete);
                }

                DialogUtil.setToggledDialog("");
                AlertUtil.success(responseMessage, () => {
                    this.props.history.push(`/athletes/${this.state.athleteService.athlete.getId()}`);
                });
            });
    }

    handleInputOnChange = () => (event: any) => {
        event.preventDefault();
        this.setState({[event.target.id]: event.target.value});
    }
}
