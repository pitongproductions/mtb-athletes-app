import React from "react";
import Athlete from "../../../models/Athlete";
import DisabledRequestingButton from "../../../components/DisabledRequestingButton";
import DialogUtil from "../../../utils/DialogUtil";
import ImageUploader from "react-images-upload";
import FileUtil from "../../../utils/FileUtil";
import AthleteService from "../../../services/AthleteService";
import AlertUtil from "../../../utils/AlertUtil";

interface Props {
    mainReducer: {
        isRequesting: boolean
    },
    athlete: Athlete,
    setAthlete: Function
}

export default class UploadImagesDialog extends React.Component<Props> {
    state = {
        athleteService: new AthleteService(),
        imageFileList: [],
        imageDataUrlList: []
    };

    constructor(props: Props) {
        super(props);
        this.onDrop = this.onDrop.bind(this);
        this.uploadImages = this.uploadImages.bind(this);
    }

    render() {
        return (
            <div className="modal fixed w-full h-full top-0 left-0 flex items-center z-20">
                <div className="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"/>
                <div className="w-full px-2 z-50">
                    <div className="modal-container bg-white sm:max-w-lg mx-auto rounded shadow-lg">
                        <div className="modal-header text-center p-6 bg-blue-500">
                            <h1 className="text-white font-bold uppercase">Upload Images</h1>
                        </div>
                        <div className="modal-content overflow-y-auto p-6 text-center">
                            <ImageUploader
                                withIcon={true}
                                label="Max file size: 5mb, accepted: jpg|jpeg|gif|png"
                                buttonText='Choose images'
                                imgExtension={['jpeg', '.jpg', '.gif', '.png']}
                                maxFileSize={5242880}
                                onChange={this.onDrop}
                                className="shadow-none"
                            />
                            <div className="mt-6">
                                {
                                    this.state.imageDataUrlList.map((dataUrl: string, index: number) => (
                                        <div
                                            key={index}
                                            className="w-1/2 inline-block p-1 relative"
                                        >
                                            <div
                                                style={FileUtil.setBackgroundImage(dataUrl)}
                                                className="p-2 rounded h-32 shadow-xl flex"
                                            >
                                                <button
                                                    type="button"
                                                    className="rounded-full h-6 w-6 shadow-xl bg-red-700 hover:bg-red-600 focus:outline-none text-white ml-auto"
                                                    onClick={() => {
                                                        this.removeImageDataUrl(index)
                                                    }}
                                                >
                                                    <i className="fa fa-times text-xs"/>
                                                </button>
                                            </div>
                                        </div>
                                    ))
                                }
                            </div>
                        </div>
                        <div className="modal-footer p-6">
                            <div className="flex">
                                <div className="flex m-auto">
                                    <div className="mr-2">
                                        {
                                            this.props.mainReducer.isRequesting ?
                                                <DisabledRequestingButton/> : <button
                                                    type="button"
                                                    className="bg-blue-500 text-white uppercase font-bold py-2 px-8 focus:outline-none rounded-full"
                                                    disabled={this.state.imageFileList.length === 0}
                                                    onClick={this.uploadImages}
                                                >
                                                    Upload
                                                </button>
                                        }
                                    </div>
                                    <button
                                        type="button"
                                        className="bg-white border border-blue-500 uppercase text-blue-500 font-bold py-2 px-8 focus:outline-none rounded-full"
                                        onClick={() => {
                                            DialogUtil.setToggledDialog("");
                                        }}
                                    >
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    onDrop(imageFileList: any) {
        let dataUrlList: string[] = [];

        this.setState({imageFileList: [...imageFileList]});

        imageFileList.forEach((image: Blob, index: number) => {
            FileUtil.readAsDataUrl(image, (dataUrl: string) => {
                dataUrlList[index] = dataUrl;
                this.setState({imageDataUrlList: dataUrlList});
            });
        });
    }

    removeImageDataUrl(indexOfImageDataUrl: number) {
        let imageFileList = this.state.imageFileList;
        let imageDataUrlList = this.state.imageDataUrlList;
        imageFileList.splice(indexOfImageDataUrl, 1);
        imageDataUrlList.splice(indexOfImageDataUrl, 1);
        this.setState({imageDataUrlList: imageDataUrlList});
    }

    uploadImages() {
        let payload = new FormData();

        this.state.imageFileList.forEach((file: Blob) => {
            payload.append("files[]", file);
        });

        this.state.athleteService.uploadAthletePhotos(this.props.athlete, payload)
            .then(() => {
                const athlete = this.state.athleteService.athlete;
                const responseMessage = this.state.athleteService.responseMessage;

                if (!athlete.isEmpty()) {
                    this.props.setAthlete(athlete);
                }

                DialogUtil.setToggledDialog("");
                AlertUtil.success(responseMessage);
            });
    }
}
