import React from "react";
import FileUtil from "../../utils/FileUtil";
import AthleteService from "../../services/AthleteService";
import Athlete from "../../models/Athlete";
import {connect} from "react-redux";
import StoreConstantDialog from "../../store/constants/StoreConstantDialog";
import DialogUtil from "../../utils/DialogUtil";
import UploadProfileOrCoverDialog from "./dialogs/UploadProfileOrCoverDialog";
import Avatar from "react-avatar";
import AddOrEditAthleteDialog from "./dialogs/AddOrEditAthleteDialog";
import AthleteAboutTab from "./components/AthleteAboutTab";
import AthletePhotosTab from "./components/AthletePhotosTab";
import AthleteSettingsTab from "./components/AthleteSettingsTab";

interface Props {
    mainReducer: {
        isRequesting: boolean
    },
    dialogReducer: Object,
    history: {
        push: Function
    }
    location: {
        search: string
    },
    match: {
        params: {
            athleteId: number
        }
    }
}

class AthleteView extends React.Component<Props> {
    state = {
        athleteService: new AthleteService(),
        athlete: new Athlete(),
        tabs: ["About", "Photos", "Settings"],
        activeTab: "About",
        dialogType: "",
        photos: [],
        videos: []
    };

    constructor(props: Props) {
        super(props);
        this.setAthlete = this.setAthlete.bind(this);
    }

    render() {
        return (
            <div className="lg:flex">
                <div className="lg:w-3/5 m-auto">
                    <div
                        style={FileUtil.setBackgroundImage(this.state.athlete.getCoverImageUrl())}
                        className="h-64 relative flex shadow-lg"
                    >
                        <div
                            className="p-4 cursor-pointer"
                            onClick={() => {
                                this.showProfileOrCoverDialog("Cover")
                            }}
                        >
                            <i className="fa fa-camera text-gray-600 hover:text-gray-700 text-2xl"/>
                        </div>
                        <div
                            style={{"bottom": "-20px"}}
                            className="sm:absolute sm:bottom-0 my-auto sm:m-auto sm:ml-8 sm:flex text-center sm:text-left"
                        >
                            <Avatar
                                name={this.state.athlete.getFullName()}
                                src={this.state.athlete.getProfileImageUrl()}
                                size="100"
                                className="rounded cursor-pointer border border-gray-500"
                                round={true}
                                onClick={() => {
                                    this.showProfileOrCoverDialog("Profile")
                                }}
                            />
                            <div className="text-gray-200 flex items-center">
                                <h1 className="text-3xl font-bold ml-3">
                                    {this.state.athlete.getFullName()}
                                </h1>
                                <button
                                    type="button"
                                    className="ml-2 focus:outline-none"
                                    onClick={() => {
                                        DialogUtil.setToggledDialog(StoreConstantDialog.ADD_OR_EDIT_ATHLETE_DIALOG);
                                    }}
                                >
                                    <i className="fa fa-edit"/>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="flex">
                        <ul className="w-full sm:w-4/5 ml-auto">
                            {
                                this.state.tabs.map((tab: string, index: number) => (
                                    <li
                                        key={index}
                                        className={`${this.isTabActive(tab) ? "bg-gray-200" : ""} w-4/12 inline-block py-1 px-4 border border-gray-300 text-center cursor-pointer`}
                                        onClick={() => this.changeTab(tab)}
                                    >
                                        {tab}
                                    </li>
                                ))
                            }
                        </ul>
                    </div>
                    <div className="p-2 lg:p-0 block mt-10">
                        <div className="py-2 ml-3">
                            <h1 className="text-2xl">{this.state.activeTab}</h1>
                        </div>
                        <div className="p-4 border border-gray-300">
                            {
                                this.isTabActive("About") ?
                                    <AthleteAboutTab athlete={this.state.athlete}/> : ""
                            }
                            {
                                this.isTabActive("Photos") ?
                                    <AthletePhotosTab
                                        mainReducer={this.props.mainReducer}
                                        dialogReducer={this.props.dialogReducer}
                                        athlete={this.state.athlete}
                                        setAthlete={this.setAthlete}
                                    /> : ""
                            }
                            {
                                this.isTabActive("Settings") ?
                                    <AthleteSettingsTab
                                        mainReducer={this.props.mainReducer}
                                        dialogReducer={this.props.dialogReducer}
                                        history={this.props.history}
                                        athlete={this.state.athlete}
                                    /> : ""
                            }
                        </div>
                    </div>
                </div>
                {
                    DialogUtil.isDialogToggled(StoreConstantDialog.UPLOAD_PROFILE_OR_COVER_IMAGE_DIALOG) ?
                        <UploadProfileOrCoverDialog
                            mainReducer={this.props.mainReducer}
                            dialogType={this.state.dialogType}
                            athlete={this.state.athlete}
                            setAthlete={this.setAthlete}
                        /> : ""
                }
                {
                    DialogUtil.isDialogToggled(StoreConstantDialog.ADD_OR_EDIT_ATHLETE_DIALOG) ?
                        <AddOrEditAthleteDialog
                            history={this.props.history}
                            mainReducer={this.props.mainReducer}
                            athlete={this.state.athlete}
                            setAthlete={this.setAthlete}
                        /> : ""
                }
            </div>
        );
    }

    componentDidMount() {
        const queryTab = this.props.location.search.split("=");

        if (queryTab.length > 1) {
            this.setState({activeTab: queryTab[1]});
        }

        this.loadAthlete();
    }

    showProfileOrCoverDialog(dialogType: string) {
        this.setState({dialogType: dialogType});
        DialogUtil.setToggledDialog(StoreConstantDialog.UPLOAD_PROFILE_OR_COVER_IMAGE_DIALOG);
    }

    isTabActive(tab: string) {
        return this.state.activeTab === tab;
    }

    changeTab(tab: string) {
        this.setState({activeTab: tab});
        this.props.history.push(`/athletes/${this.athleteId()}?tab=${tab}`);
    }

    athleteId() {
        return this.props.match.params.athleteId;
    }

    setAthlete(athlete: Athlete) {
        this.setState({athlete: athlete});
    }

    loadAthlete() {
        this.state.athleteService.getAthlete(this.athleteId())
            .then(() => {
                const athlete = this.state.athleteService.athlete;

                if (athlete.isEmpty()) {
                    this.props.history.push("/");
                }

                this.setAthlete(athlete);
            });
    }
}

const mapStateToProps = (state: any) => ({
    mainReducer: state.mainReducer,
    dialogReducer: state.dialogReducer
});

export default connect(mapStateToProps, null)(AthleteView);
