import "nprogress/nprogress.css";
import axios from "axios";
import store from "../store/Store";
import StoreActionMain from "../store/actions/StoreActionMain";
import {throttleAdapterEnhancer} from "axios-extensions";
import NProgress from "nprogress";
import Session from "../session/Session";
import AlertUtil from "../utils/AlertUtil";

NProgress.configure({showSpinner: false});

class Http {

    readonly USER_ACCESS_TOKEN: string = Session.getUserAccessToken();
    readonly ADAPTER: any = axios.defaults.adapter;
    readonly APIS: any = {
        remote: "",
        development: process.env.REACT_APP_LOCAL_API_URL,
        staging: process.env.REACT_APP_STAGING_API_URL,
        production: process.env.REACT_APP_PRODUCTION_API_URL,
    };

    instance() {
        let request: any = {
            baseURL: `${this.APIS[process.env.NODE_ENV]}/api/`,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": `Bearer ${this.USER_ACCESS_TOKEN}`
            },
            adapter: throttleAdapterEnhancer(this.ADAPTER, {
                threshold: 2000
            })
        };

        const instance: any = axios.create(request);

        instance.interceptors.request.use((config: any) => {
            NProgress.start();
            store.dispatch(StoreActionMain.setRequestingStatus(true));
            return config;
        }, () => {
            store.dispatch(StoreActionMain.setRequestingStatus(false));
            NProgress.done();
        });

        instance.interceptors.response.use((response: any) => {
            NProgress.done();
            store.dispatch(StoreActionMain.setRequestingStatus(false));
            return response;
        }, (error: any) => {
            if (!error) {
                return;
            }

            const errorMessage = error.response.data.error;

            if (errorMessage) {
                AlertUtil.error(errorMessage);
            }

            store.dispatch(StoreActionMain.setRequestingStatus(false));
            NProgress.done();
        });

        return instance;
    }
}

export default new Http().instance();
