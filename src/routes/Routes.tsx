import AthleteList from "../templates/athlete/AthleteList";
import AthleteView from "../templates/athlete/AthleteView";
import Error404 from "../templates/error/Error404";
import MapInterface from "../interfaces/MapInterface";

export default class Routes {
    static config: MapInterface = [
        {
            path: "/",
            component: AthleteList,
            exact: true
        },
        {
            path: "/athletes/:athleteId",
            component: AthleteView
        },
        {
            path: "*",
            component: Error404
        }
    ]
}
