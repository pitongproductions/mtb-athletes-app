import Http from "../http/Http";
import Athlete from "../models/Athlete";
import RequestMethodConstant from "../constants/RequestMethodConstant";
import Pagination from "../models/Pagination";
import AthleteInterface from "../interfaces/AthleteInterface";

export default class AthleteService {

    public athleteList: Array<Athlete>;
    public athlete: Athlete;
    public pagination: Pagination;
    public responseMessage: string;

    constructor() {
        this.athleteList = new Array<Athlete>();
        this.athlete = new Athlete();
        this.pagination = new Pagination();
        this.responseMessage = "";
    }

    async uploadAthletePhotos(selectedAthlete: Athlete, payload: object) {
        const response: any = await Http({
            url: `athletes/${selectedAthlete.getId()}/photos`,
            method: RequestMethodConstant.POST,
            data: payload
        }, RequestMethodConstant.HTTP_HEADER_MULTIPART);

        if (!response) {
            return;
        }

        const {data} = response;
        const {athlete, success} = data;

        this.athlete = new Athlete().toObject(athlete);
        this.responseMessage = success;

        return response;
    }

    async uploadAthleteProfileImage(selectedAthlete: Athlete, payload: any) {
        const response: any = await Http({
            url: `athletes/${selectedAthlete.getId()}/profileOrCover`,
            method: RequestMethodConstant.POST,
            data: payload
        }, RequestMethodConstant.HTTP_HEADER_MULTIPART);

        if (!response) {
            return;
        }

        const {data} = response;
        const {athlete, success} = data;

        this.athlete = new Athlete().toObject(athlete);
        this.responseMessage = success;

        return response;
    }

    async deleteAthletePhoto(selectedAthlete: Athlete, payload: any) {
        const response: any = await Http({
            url: `athletes/${selectedAthlete.getId()}/photos`,
            method: RequestMethodConstant.DELETE,
            data: payload
        });

        if (!response) {
            return;
        }

        const {data} = response;
        const {athlete, success} = data;

        this.athlete = new Athlete().toObject(athlete);
        this.responseMessage = success ?? this.responseMessage;
    }

    async deleteAthlete(selectedAthlete: Athlete) {
        const response: any = await Http({
            url: `athletes/${selectedAthlete.getId()}`,
            method: RequestMethodConstant.DELETE
        });

        if (!response) {
            return;
        }

        const {data} = response;
        const {success} = data;

        this.responseMessage = success;

        return response;
    }

    async saveAthlete(payload: any) {
        let url = "athletes";
        let method = RequestMethodConstant.POST;

        if ("id" in payload && payload.id > 0) {
            url += `/${payload.id}`;
            method = RequestMethodConstant.PUT;
        }

        const response: any = await Http({
            url: url,
            method: method,
            data: payload
        });

        if (!response) {
            return;
        }

        const {data} = response;
        const {athlete, success} = data;

        this.athlete = new Athlete().toObject(athlete);
        this.responseMessage = success ?? this.responseMessage;

        return response;
    }

    async getAthlete(athleteId: number) {
        const response: any = await Http({
            url: `athletes/${athleteId}`,
            method: RequestMethodConstant.GET
        });

        if (!response) {
            return;
        }

        const {data} = response;
        const {athlete} = data;

        this.athlete = new Athlete().toObject(athlete);

        return response;
    }

    async getAthletes(payload: any) {
        const response: any = await Http({
            url: "athletes",
            method: RequestMethodConstant.GET,
            params: payload
        });

        if (!response) {
            return;
        }

        const {data} = response;
        const {athletes} = data;

        this.athleteList = new Array<Athlete>();
        this.pagination = new Pagination().toObject(athletes);

        athletes.data.forEach((athlete: AthleteInterface) => {
            this.athleteList.push(new Athlete().toObject(athlete));
        });

        return response;
    }
}
