import Store from "../store/Store";
import StoreActionDialog from "../store/actions/StoreActionDialog";

export default class DialogUtil {

    static setToggledDialog(dialogName: string) {
        Store.dispatch(StoreActionDialog.setToggledDialog(dialogName));
    }

    static isDialogToggled(dialogName: string) {
        const {dialogReducer} = Store.getState();
        return dialogReducer.toggledDialog === dialogName;
    }
}
