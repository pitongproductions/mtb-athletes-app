const ASSETS_DIRECTORY = "assets";

export default class FileUtil {

    static setBackgroundImage(imageUrl: string) {
        return {
            backgroundImage: `url(${imageUrl})`,
            backgroundPosition: "contain",
            backgroundRepeat: "no-repeat",
            backgroundSize: "100% 100%"
        }
    }

    static getFileFromAssets(fileString: string) {
        return require(`../${ASSETS_DIRECTORY}/${fileString}`);
    }

    static urlToFile(uri: string, fileName: string, fileType: string) {
        return new File([this.urlToBlob(uri)], fileName, {type: fileType});
    }

    static urlToBlob(dataUrl: string) {

        let dataUrlList = dataUrl.split(',');
        const mimeList = dataUrlList[0].match(/:(.*?);/) ?? [];
        let mime = mimeList[1];
        let bytes = atob(dataUrlList[1]);
        let lengthOfBytes = bytes.length;
        let u8arr = new Uint8Array(lengthOfBytes);

        while (lengthOfBytes--) {
            u8arr[lengthOfBytes] = bytes.charCodeAt(lengthOfBytes);
        }

        return new Blob([u8arr], {type: mime});
    }

    static readAsDataUrl(file: Blob, callback: Function = () => {}) {
        const fileReader = new FileReader();

        if (!callback) {
            return;
        }

        fileReader.addEventListener("load", () => {
            callback(fileReader.result);
        });

        fileReader.readAsDataURL(file);
    }
}
