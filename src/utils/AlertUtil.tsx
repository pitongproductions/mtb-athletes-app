import Swal from "sweetalert2";
import AppConstant from "../constants/AppConstant";

export default class AlertUtil {

    static readonly SUCCESS: string = "success";
    static readonly ERROR: string = "error";

    static success(message: string, callback: Function = () => {}) {
        showAlert(message, this.SUCCESS, callback);
    }

    static error(message: string) {
        showAlert(message, this.ERROR);
    }
}

function showAlert(message: string, icon: string, callback: Function = () => {}) {
    let alert: any = {
        icon: icon,
        title: message,
        showConfirmButton: false,
        timer: AppConstant.DEFAULT_SWEET_ALERT_TIMER
    };

    if (callback) {
        alert.willClose = callback;
    }

    Swal.fire(alert);
}
