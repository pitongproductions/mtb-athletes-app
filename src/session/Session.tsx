export default class Session {

    static USER_ACCESS_TOKEN: string = process.env.REACT_APP_ACCESS_TOKEN_KEY ?? "";

    static setUserAccessToken(token: string) {
        localStorage.setItem(this.USER_ACCESS_TOKEN, token);
    }

    static getUserAccessToken() {
        return localStorage.getItem(this.USER_ACCESS_TOKEN) ?? "";
    }

    static removeUserAccessToken() {
        localStorage.removeItem(this.USER_ACCESS_TOKEN);
    }
}
