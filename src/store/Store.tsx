import Reducers from "./reducers/Reducers";
import {createStore} from "redux";

class Store {
    static readonly CONFIG: any = createStore(Reducers);
}

export default Store.CONFIG;
