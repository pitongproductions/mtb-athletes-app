import StoreTypeDialog from "../types/StoreTypeDialog";

class StoreActionDialog {

    static setToggledDialog(dialog: string) {
        return {
            type: StoreTypeDialog.SET_TOGGLED_DIALOG,
            payload: dialog
        }
    }
}

export default StoreActionDialog;


