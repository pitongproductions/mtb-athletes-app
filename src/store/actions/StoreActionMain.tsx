import StoreTypeMain from "../types/StoreTypeMain";

class StoreActionMain {

    static setRequestingStatus(status: boolean) {
        return {
            type: StoreTypeMain.SET_REQUESTING_STATUS,
            payload: status
        }
    }
}

export default StoreActionMain;
