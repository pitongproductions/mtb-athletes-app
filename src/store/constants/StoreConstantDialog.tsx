export default class StoreConstantDialog {
    static readonly ADD_OR_EDIT_ATHLETE_DIALOG = "ADD_OR_EDIT_ATHLETE_DIALOG";
    static readonly UPLOAD_PROFILE_OR_COVER_IMAGE_DIALOG = "UPLOAD_PROFILE_OR_COVER_IMAGE_DIALOG";
    static readonly UPLOAD_IMAGES = "UPLOAD_IMAGES";
    static readonly DELETE_ATHLETE_DIALOG = "DELETE_ATHLETE_DIALOG";
    static readonly VIEW_PHOTOS_DIALOG = "VIEW_PHOTOS_DIALOG";
}
