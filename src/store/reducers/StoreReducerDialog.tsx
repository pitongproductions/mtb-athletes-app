import StoreTypeMain from "../types/StoreTypeDialog";

let initialState = {
    toggledDialog: ""
};

let actionState = {
    type: "",
    payload: null
};

class StoreReducerDialog {

    static dialogReducer(state = initialState, action = actionState) {
        switch (action.type) {
            case StoreTypeMain.SET_TOGGLED_DIALOG:
                return {...state, toggledDialog: action.payload};
            default:
                return state;
        }
    }
}

export default StoreReducerDialog.dialogReducer;
