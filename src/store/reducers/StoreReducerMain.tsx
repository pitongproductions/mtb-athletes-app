import StoreTypeMain from "../types/StoreTypeMain";

let initialState = {
    isRequesting: false
};

let actionState = {
    type: "",
    payload: null
};

class StoreReducerMain {
    static mainReducer = (state = initialState, action = actionState) => {
        switch (action.type) {
            case StoreTypeMain.SET_REQUESTING_STATUS:
                return {...state, isRequesting: action.payload};
            default:
                return state;
        }
    }
}

export default StoreReducerMain.mainReducer;
