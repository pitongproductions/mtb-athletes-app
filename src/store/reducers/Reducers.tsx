import {combineReducers} from "redux";
import mainReducer from "./StoreReducerMain";
import dialogReducer from "./StoreReducerDialog";

class Reducers {

    static readonly CONFIG: any = combineReducers({
        mainReducer,
        dialogReducer
    });
}

export default Reducers.CONFIG;
